package com.edwinnyawoli.geniuscrosswords.view;

import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by ed on 4/22/2016.
 */
public class CrosswordKeyboardView {
    public static final int KEYCODE_DEL = 127;
    private KeyboardView keyboardView;
    private KeyboardView.OnKeyboardActionListener actionListener;
    private OnKeyPressListener onKeyPressListener;

    public CrosswordKeyboardView(AppCompatActivity activity, int keyboardViewId, int keyboardId) {
        keyboardView = (KeyboardView) activity.findViewById(keyboardViewId);
        keyboardView.setPreviewEnabled(false);


        keyboardView.setKeyboard(new Keyboard(activity, keyboardId));

        initListeners();
        keyboardView.setOnKeyboardActionListener(actionListener);

    }

    public void setOnKeyPressListener(OnKeyPressListener l) {
        onKeyPressListener = l;
    }

    private void initListeners() {
        actionListener = new KeyboardView.OnKeyboardActionListener() {
            @Override
            public void onPress(int i) {
            }

            @Override
            public void onRelease(int i) {

            }

            @Override
            public void onKey(int i, int[] ints) {
                if (onKeyPressListener != null)
                    onKeyPressListener.onKey(i);
            }

            @Override
            public void onText(CharSequence charSequence) {

            }

            @Override
            public void swipeLeft() {

            }

            @Override
            public void swipeRight() {

            }

            @Override
            public void swipeDown() {

            }

            @Override
            public void swipeUp() {

            }
        };
    }

    public interface OnKeyPressListener {
        void onKey(int code);
    }

}
