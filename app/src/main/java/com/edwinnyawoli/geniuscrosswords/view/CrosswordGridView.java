package com.edwinnyawoli.geniuscrosswords.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.edwinnyawoli.geniuscrosswords.model.Grid;
import com.edwinnyawoli.geniuscrosswords.model.Word;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by ed on 4/20/2016.
 */
public class CrosswordGridView extends View implements View.OnTouchListener {
//    TODO Once a word has been found lets cross it clue from the clue list.
    /**
     * The paints used by CellViews when being rendered based on their current state.
     */
    private static final Paint CELLVIEW_NORMAL_BACKGROUND_PAINT;
    private static final Paint CELLVIEW_NORMAL_BORDER_PAINT;
    private static final Paint CELLVIEW_FOCUSED_BACKGROUND_PAINT;
    private static final Paint CELLVIEW_FOCUSED_BORDER_PAINT;
    private static final Paint CELLVIEW_HIGHLIGHTED_BACKGROUND_PAINT;
    private static final Paint CELLVIEW_HIGHLIGHTED_BORDER_PAINT;
    private static final Paint CELLVIEW_TEXT_FONT_PAINT;
    private static final Paint CELLVIEW_NUMBER_FONT_PAINT;
    private static final String TAG = "CrosswordGridView";

    private static final int NORMAL_ALPHA;
    private static final int HIGHLIGHT_ALPHA;

    static {

        //TODO: Update code to only change border colour when cell is focused.
        NORMAL_ALPHA = 155;
        HIGHLIGHT_ALPHA = 200;

        //Cell number and text font paints and its properties
        CELLVIEW_NUMBER_FONT_PAINT = new Paint(Paint.HINTING_ON | Paint.LINEAR_TEXT_FLAG);
        CELLVIEW_NUMBER_FONT_PAINT.setColor(Color.BLACK);
        CELLVIEW_NUMBER_FONT_PAINT.setTextAlign(Paint.Align.LEFT);

        CELLVIEW_TEXT_FONT_PAINT = new Paint(Paint.HINTING_ON | Paint.LINEAR_TEXT_FLAG);
        CELLVIEW_TEXT_FONT_PAINT.setColor(Color.BLACK);
        CELLVIEW_TEXT_FONT_PAINT.setTextAlign(Paint.Align.CENTER);


        //Normal Background Paint and its properties
        CELLVIEW_NORMAL_BACKGROUND_PAINT = new Paint();
        CELLVIEW_NORMAL_BACKGROUND_PAINT.setColor(Color.WHITE);
        CELLVIEW_NORMAL_BACKGROUND_PAINT.setAlpha(NORMAL_ALPHA);

        //Normal Border Paint and its properties
        CELLVIEW_NORMAL_BORDER_PAINT = new Paint();
        CELLVIEW_NORMAL_BORDER_PAINT.setColor(Color.BLACK);

        //Setting text size on this paint because it is being used to draw text


        //Focused Background Paint and its properties
        CELLVIEW_FOCUSED_BACKGROUND_PAINT = new Paint();
        CELLVIEW_FOCUSED_BACKGROUND_PAINT.setColor(Color.WHITE);

        //Focused Border Paint and its properties
        CELLVIEW_FOCUSED_BORDER_PAINT = CELLVIEW_NORMAL_BORDER_PAINT;

        //Highlighted Background Paint and its properties
        CELLVIEW_HIGHLIGHTED_BACKGROUND_PAINT = new Paint();
        CELLVIEW_HIGHLIGHTED_BACKGROUND_PAINT.setColor(Color.BLUE);
        CELLVIEW_HIGHLIGHTED_BACKGROUND_PAINT.setAlpha(HIGHLIGHT_ALPHA);

        //Hightlighted Border Paint and its properties
        CELLVIEW_HIGHLIGHTED_BORDER_PAINT = CELLVIEW_NORMAL_BORDER_PAINT;
    }

    private float cellWidth;
    private float cellHeight;
    private Grid mGrid;
    /*
    The direction in which to highlight juxtaposed CellViews or to follow when moving focus
    to another CellView.
    */
    private Direction direction;
    private ArrayList<Word> wordList;
    //The previously focused cellView
    private CellView mPreviousFocusCellView;
    //The currently focused cellView
    private CellView mFocusedCellView;
    private LinkedList<CellView> mHighlightedCellViews;
    private CellViewFocusListener cellViewFocusListener;
    private OnWordCellViewCompletedListener onWordCellViewCompletedListener;
    //The buckets that would contain the CellViews on which processing would be perfomred
    private CellView[] horizontalCellViewBucket, verticalCellViewBucket;
    private boolean buttonCausedDirectionToggle;
    //Represents the number of inputted characters for the word the user is trying to solve.
    private int inputWordCharCount;


    /**
     * An array list of linked lists that holds cells of a particular row.
     * Each array list index is a row in the grid.
     */
    private ArrayList<LinkedList<CellView>> mCellRows;
    private boolean requestInvalidationAfterLayout;

    public CrosswordGridView(Context context) {
        super(context);
        initGridView();
    }

    public CrosswordGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initGridView();
    }

    /**
     * Toggles the direction between Horizontal and Vertical.
     */
    private void toggleDirection() {
        direction = (direction == Direction.HORIZONTAL) ? Direction.VERTICAL : Direction.HORIZONTAL;
        inputWordCharCount = 0;
    }

    private void initGridView() {
        direction = Direction.HORIZONTAL;
        inputWordCharCount = 0;
        mHighlightedCellViews = new LinkedList<>();
        setOnTouchListener(this);
    }

    public void setGrid(Grid grid) {
        mGrid = grid;
        if (grid != null) {
            wordList = grid.getWordList();
            //Create the buckets with the maximum size of their respective grid orientation.
            horizontalCellViewBucket = new CellView[mGrid.getColumnCount()];
            verticalCellViewBucket = new CellView[mGrid.getRowCount()];
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        recreateCellViews();
    }

    /**
     * Recreates the CellViews to be displayed in the grid.
     */
    private void recreateCellViews() {
        if (mGrid == null)
            return;

        updateFontSizes();

        mCellRows = new ArrayList<>(mGrid.getRowCount());
//        Add the required number  of linked lists for each row.
        for (int i = 0; i < mGrid.getRowCount(); i++) {
            mCellRows.add(new LinkedList<CellView>());
        }

        for (int i = 0; i < wordList.size(); i++) {
            Word word = wordList.get(i);
            int startX = word.getStartingX();
            int startY = word.getStartingY();

            int clueNumber = word.getClueNumber();
            int wordNumber = word.getWordNumber();

//            Screen x and y coordinates
            float x = ((float) startX) * cellWidth;
            float y = ((float) startY) * cellHeight;

//            Get the startY position row of cells
            LinkedList<CellView> row = mCellRows.get(startY);

//            For every character in the word
            for (int j = 0; j < word.getLength(); j++) {
                if (word.isHorizontal()) {
                    x = (startX + j) * cellWidth;
                    CellView cv = new CellView(x, y, wordNumber, clueNumber);
                    int existingCVIndex = row.indexOf(cv);
                    if (existingCVIndex != -1)
                        row.get(existingCVIndex).setClueNumber(clueNumber);
                    else
                        row.add(cv);
                } else {
                    y = (startY + j) * cellHeight;
//                    Get the next row
                    LinkedList<CellView> rowDown = mCellRows.get(startY + j);
                    CellView cv = new CellView(x, y, wordNumber, clueNumber);
                    int existingCVIndex = rowDown.indexOf(cv);
                    if (existingCVIndex != -1)
                        rowDown.get(existingCVIndex).setClueNumber(clueNumber);
                    else
                        rowDown.add(cv);
                }

                clueNumber = -1;
            }
        }
    }

    private void updateFontSizes() {
        CELLVIEW_TEXT_FONT_PAINT.setTextSize(cellHeight - 5);
        CELLVIEW_NUMBER_FONT_PAINT.setTextSize(cellHeight / 4);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawRect(0, 0, cellWidth * mGrid.getColumnCount(), cellHeight * mGrid.getRowCount(), CELLVIEW_FOCUSED_BORDER_PAINT);

        super.onDraw(canvas);
        if (mCellRows != null) {
            for (int i = 0; i < mCellRows.size(); i++) {
                LinkedList<CellView> row = mCellRows.get(i);
                for (int j = 0; j < row.size(); j++) {
                    CellView cellView = row.get(j);
                    cellView.onDraw(canvas);
                }
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        if (mGrid != null) {
            cellWidth = width / mGrid.getColumnCount();
            cellHeight = height / mGrid.getColumnCount();
        }

        setMeasuredDimension(width, height);
    }

    public void setCellViewFocusListener(CellViewFocusListener l) {
        cellViewFocusListener = l;
    }

    /**
     * Moves focus from the currently focused CellView to the next focusable CellView
     * based on the current direction
     */
    private void forwardFocus() {
        int focusYIndex = getVerticalIndexOfPosition(mFocusedCellView.y);
        int focusXIndex = getHorizontalIndexOfPosition(mFocusedCellView.x);

        switch (direction) {
            case HORIZONTAL:
                setFocusedCellView(getCellViewAt(focusXIndex + 1, focusYIndex));
                break;

            case VERTICAL:
                setFocusedCellView(getCellViewAt(focusXIndex, focusYIndex + 1));
                break;
        }
    }

    private void reverseFocus() {
        if (mFocusedCellView == null)
            return;

        int focusYIndex = getVerticalIndexOfPosition(mFocusedCellView.y);
        int focusXIndex = getHorizontalIndexOfPosition(mFocusedCellView.x);

        switch (direction) {
            case HORIZONTAL:
                setFocusedCellView(getCellViewAt(focusXIndex - 1, focusYIndex));
                break;

            case VERTICAL:
                setFocusedCellView(getCellViewAt(focusXIndex, focusYIndex - 1));
                break;
        }
    }

    /**
     * @return Whether there are CellViews that share boundaries with the focused CellView
     * either on its left or right.
     */
    private boolean hasJuxtaposedHorizontalSiblings() {
        int focusYIndex = getVerticalIndexOfPosition(mFocusedCellView.y);
        int focusXIndex = getHorizontalIndexOfPosition(mFocusedCellView.x);

        return (getCellViewAt(focusXIndex + 1, focusYIndex) != null) || (getCellViewAt(focusXIndex - 1, focusYIndex) != null);
    }

    /**
     * @return Whether there are CellViews that share boundaries with the focused CellView by
     * being either above or below it.
     */
    private boolean hasJuxtaposedVerticalSiblings() {

        int focusYIndex = getVerticalIndexOfPosition(mFocusedCellView.y);
        int focusXIndex = getHorizontalIndexOfPosition(mFocusedCellView.x);

        return (getCellViewAt(focusXIndex, focusYIndex - 1) != null) || (getCellViewAt(focusXIndex, focusYIndex + 1) != null);
    }

    /**
     * Makes the given view the focused view and notifies listeners and makes internal changes accordingly.
     *
     * @param newFocusedView The view to change focus to.
     */
    private void setFocusedCellView(CellView newFocusedView) {
        //We don't want to be doing anything with nulls.
        if (mPreviousFocusCellView == null && newFocusedView == null)
            return;

        //If the focused view hasnt actually changed.
        if (mFocusedCellView == newFocusedView) {
            toggleDirectionIfRequired();
        } else {
            if (mPreviousFocusCellView != null)
                mPreviousFocusCellView.setFocused(false);

            //Do the swap dance
            mFocusedCellView = newFocusedView;
            mPreviousFocusCellView = mFocusedCellView;

            if (mFocusedCellView != null) {
                toggleDirectionIfRequired();
            }
            notifyCellViewFocusChanged(mFocusedCellView, mPreviousFocusCellView);
        }

        restoreHighlightedCellViews();
        readCellViewsForWordWithFocusCellView();
        highlightJuxtaposedSiblingViews();

        if (mFocusedCellView != null)
            mFocusedCellView.setFocused(true);
    }

    /**
     * Clears both buckets (Horizontal and Vertical) and populates them with the CellViews of the
     * word that contains the focused CellView.
     */
    private void readCellViewsForWordWithFocusCellView() {
//        Clear both buckets
        for (int i = 0; i < horizontalCellViewBucket.length; i++) {
            horizontalCellViewBucket[i] = null;
        }

        for (int i = 0; i < verticalCellViewBucket.length; i++) {
            verticalCellViewBucket[i] = null;
        }

        if (mFocusedCellView == null)
            return;

        int focusIndexX = getHorizontalIndexOfPosition(mFocusedCellView.x);
        int focusIndexY = getVerticalIndexOfPosition(mFocusedCellView.y);

        if (hasJuxtaposedHorizontalSiblings()) {
            int leftIndexPtr = focusIndexX - 1, rightIndexPtr = focusIndexX + 1;
            boolean goLeft = true, goRight = true;

            while (goLeft || goRight) {
                if (goLeft) {
                    if (getCellViewAt(leftIndexPtr, focusIndexY) == null)
                        goLeft = false;
                    else
                        leftIndexPtr--;
                }

                if (goRight) {
                    if (getCellViewAt(rightIndexPtr, focusIndexY) == null)
                        goRight = false;
                    else
                        rightIndexPtr++;
                }
            }

//            Adjust the counter to move from the nulls
            leftIndexPtr++;
            rightIndexPtr--;

            for (int x = leftIndexPtr, i = 0; x <= rightIndexPtr; x++, i++) {
                horizontalCellViewBucket[i] = getCellViewAt(x, focusIndexY);
            }
        }

        if (hasJuxtaposedVerticalSiblings()) {
            int upIndexPtr = focusIndexY - 1, downIndexPtr = focusIndexY + 1;
            boolean goUp = true, goDown = true;

            while (goUp || goDown) {
                if (goUp) {
                    if (getCellViewAt(focusIndexX, upIndexPtr) == null)
                        goUp = false;
                    else
                        upIndexPtr--;
                }

                if (goDown) {
                    if (getCellViewAt(focusIndexX, downIndexPtr) == null)
                        goDown = false;
                    else
                        downIndexPtr++;
                }
            }

//            Adjust counters to move from nulls
            upIndexPtr++;
            downIndexPtr--;

            for (int y = upIndexPtr, i = 0; y <= downIndexPtr; y++, i++) {
                verticalCellViewBucket[i] = getCellViewAt(focusIndexX, y);
            }
        }
    }

    /**
     * Validates the content of the CellViews of the word(s) which has the focused CellViews
     * as one of its CellViews.
     */
    private void requestValidateWordsCellViewContent() {
        //Validate if available, either the row or column or both
//        getCellViewsForWordsHavingFocusedCellView
//        Validate their content
        int wordIndex = -1;
        boolean isValidFill = true;
        char[] wordChars = null;
        CellView[] cellViewBucket = null;

        switch (direction) {
            case HORIZONTAL:
                cellViewBucket = horizontalCellViewBucket;
                break;
            case VERTICAL:
                cellViewBucket = verticalCellViewBucket;
                break;
        }

        Log.d(TAG, "Displaying Horizontal CellBucket Content: ");
        Log.d(TAG, "-----------------------------------------");
        for (int i = 0; i < horizontalCellViewBucket.length; i++) {
            Log.d(TAG, i + " -> " + horizontalCellViewBucket[i]);
        }
        Log.d(TAG, "-----------------------------------------");

        Log.d(TAG, "Displaying Vertical CellBucket Content: ");
        Log.d(TAG, "-----------------------------------------");
        for (int i = 0; i < verticalCellViewBucket.length; i++) {
            Log.d(TAG, i + " -> " + verticalCellViewBucket[i]);
        }
        Log.d(TAG, "-----------------------------------------");

        if (cellViewBucket[0] == null) {
            Toast.makeText(getContext(), "An error occured", Toast.LENGTH_SHORT).show();
            return;
        }

        wordIndex = cellViewBucket[0].wordNumber;
        wordChars = wordList.get(wordIndex - 1).getWord().toCharArray();

        if (wordChars != null) {
            for (int i = 0; i < wordChars.length; i++) {
                if (wordChars[i] != cellViewBucket[i].getCharacter()) {
                    Log.d(TAG, "Word char: " + wordChars[i] + " and cellView char: " + cellViewBucket[i].getCharacter());
                    isValidFill = false;
                    break;
                }
            }

            if (isValidFill) {
                Log.d(TAG, "CellViews completion valid.");
                Toast.makeText(getContext(), "You found a word", Toast.LENGTH_SHORT).show();
            } else {
                Log.d(TAG, "CellViews completion invalid");
            }
        }
    }

    /**
     * Toggles the direction if the current direction is not the best direction to be used.
     * <p>
     * An example of a scenario where this may happen are when the current direction is horizontal
     * but the focused CellView has only vertical juxtaposed siblings.</p>
     * <p>
     * Another example is when an intersecting CellView is tapped twice.
     * </p>
     */
    private void toggleDirectionIfRequired() {
        if (!buttonCausedDirectionToggle)
            return;

//        TODO Fix the toggling . We dont want to change direction at every intersection.
        if (direction == Direction.HORIZONTAL && hasJuxtaposedVerticalSiblings())
            toggleDirection();
        else if (direction == Direction.VERTICAL && hasJuxtaposedHorizontalSiblings())
            toggleDirection();
    }

    private void highlightJuxtaposedSiblingViews() {
        if (mFocusedCellView == null)
            return;

        switch (direction) {
            case HORIZONTAL:
                for (int i = 0; i < horizontalCellViewBucket.length; i++) {
                    if (horizontalCellViewBucket[i] != null)
                        horizontalCellViewBucket[i].setHightlighted(true);
                }
                break;

            case VERTICAL:
                for (int i = 0; i < verticalCellViewBucket.length; i++) {
                    if (verticalCellViewBucket[i] != null)
                        verticalCellViewBucket[i].setHightlighted(true);

                }
                break;
        }
    }

    private void restoreHighlightedCellViews() {
        for (int i = 0; i < horizontalCellViewBucket.length; i++) {
            if (horizontalCellViewBucket[i] != null)
                horizontalCellViewBucket[i].setHightlighted(false);
        }

        for (int i = 0; i < verticalCellViewBucket.length; i++) {
            if (verticalCellViewBucket[i] != null)
                verticalCellViewBucket[i].setHightlighted(false);
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        float touchX = motionEvent.getX();
        float touchY = motionEvent.getY();

        int indexX = getHorizontalIndexOfPosition(touchX);
        int indexY = getVerticalIndexOfPosition(touchY);

//        Log.d(TAG, "X:" + touchX + ", Y:" + touchY);
//        Log.d(TAG, "Touch index: " + indexX + ", " + indexY);

        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                CellView touchedView = getCellViewAt(indexX, indexY);
//                Log.d(TAG, "CellView touched: " + touchedView);
                buttonCausedDirectionToggle = true;
                setFocusedCellView(touchedView);
                buttonCausedDirectionToggle = false;
                break;
        }
        return true;
    }

    private void notifyCellViewFocusChanged(CellView focusedView, CellView previousFocusedView) {
        if (cellViewFocusListener != null)
            cellViewFocusListener.onFocusChange(previousFocusedView, focusedView);
    }

    /**
     * @param x
     * @return The index of the horizontal position specified.
     */
    private int getHorizontalIndexOfPosition(float x) {
        return (int) (x / cellWidth);
    }

    /**
     * @param y
     * @return The index of the vertical position specified.
     */
    private int getVerticalIndexOfPosition(float y) {
        return (int) (y / cellHeight);
    }

    /**
     * @param indexX The horizontal positional index.
     * @param indexY The vertical positional index.
     * @return the CellView at the specified indexX and indexY coordinates.
     */
    private CellView getCellViewAt(int indexX, int indexY) {
        //Validate to make sure the indexes are within range
        if (validateIndexConstraints(indexX, indexY)) {
            try {
                LinkedList<CellView> row = mCellRows.get(indexY);
//                Log.d(TAG, "row " + indexY + " size: " + row.size());
                for (int i = 0; i < row.size(); i++) {
                    CellView cellView = row.get(i);
                    if (getHorizontalIndexOfPosition(cellView.x) == indexX)
                        return cellView;
                }
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    private Word getWordWithStartingIndices(int startX, int startY) {
        for (int i = 0; i < wordList.size(); i++) {
            Word w = wordList.get(i);
            if (w.getStartingX() == startX && w.getStartingY() == startY)
                return w;
        }
        return null;
    }

    private boolean validateIndexConstraints(int indexX, int indexY) {
        boolean isValid = true;
        if (indexX < 0 || indexY < 0)
            return false;
        else if (indexX >= mGrid.getColumnCount() || indexY >= mGrid.getRowCount())
            return false;
        return isValid;
    }


    /**
     * The states in which a CellView can be in.
     */
    public enum GridCellState {
        NORMAL,
        FOCUSED, //The CellView has been touched.
        HIGHLIGHTED //The cell is an adjoining CellView to the focused cell view in a particular direction.
    }

    /**
     * The direction of an adjoining CellView.
     */
    private enum Direction {
        HORIZONTAL, //above and or below.
        VERTICAL, //left and or right.
    }

    /**
     * A listener interface implemented by all who want to be notified when the CellViews
     * of a word in the Grid is completely filled, whether valid or invalid.
     */
    public interface OnWordCellViewCompletedListener {
        void onWordCompleted(boolean isValidCompletion);
    }


    public interface CellViewFocusListener {
        void onFocusChange(CellView lostFocusView, CellView gainedFocusView);
    }

    public class CellView {
        private static final String EMPTY = " ";

        private GridCellState state;
        //        Screen x and y coordinates
        private float x;
        private float y;
        private String c;
        private int clueNumber;
        private int wordNumber;

        public CellView(float x, float y, int wordNumber, int clueNumber) {
            this(x, y, EMPTY, wordNumber, clueNumber);
        }

        public CellView(float x, float y, String s, int wordNumber, int clueNumber) {
            this.x = x;
            this.y = y;
            this.c = s;
            this.clueNumber = clueNumber;
            this.wordNumber = wordNumber;
            state = GridCellState.NORMAL;
        }

        public char getCharacter() {
            return c.charAt(0);
        }

        public void setCharacter(char c) {
            if (!this.c.equals(String.valueOf(c))) {
                this.c = String.valueOf(c);
                postInvalidate((int) x, (int) y, (int) (x + cellWidth), (int) (y + cellHeight));
                if (inputWordCharCount < 0)
                    inputWordCharCount = 1;
                else
                    inputWordCharCount++;
                requestValidateWordsCellViewContent();
                forwardFocus();
            }
        }

        public void clearCharacter() {
            setCharacter(' ');
            inputWordCharCount--;
            reverseFocus();
        }

        public void setHightlighted(boolean highlighted) {
            state = highlighted ? GridCellState.HIGHLIGHTED : GridCellState.NORMAL;
            postInvalidate((int) x, (int) y, (int) (x + cellWidth), (int) (y + cellHeight));
        }

        public void setFocused(boolean focused) {
            Log.d(TAG, "View focused? " + focused);
            state = focused ? GridCellState.FOCUSED : GridCellState.NORMAL;
            postInvalidate((int) x, (int) y, (int) (x + cellWidth), (int) (y + cellHeight));
        }

        public void setClueNumber(int number) {
            if (clueNumber == -1)
                clueNumber = number;
        }

        public void setWordNumber(int number) {
            wordNumber = number;
        }


        private Paint getBackgroundPaint() {
            switch (state) {
                case HIGHLIGHTED:
                    return CELLVIEW_HIGHLIGHTED_BACKGROUND_PAINT;

                case FOCUSED:
                    return CELLVIEW_FOCUSED_BACKGROUND_PAINT;

                case NORMAL:
                default:
                    return CELLVIEW_NORMAL_BACKGROUND_PAINT;
            }
        }

        private Paint getBorderPaint() {
            switch (state) {
                case HIGHLIGHTED:
                    return CELLVIEW_HIGHLIGHTED_BORDER_PAINT;

                case FOCUSED:
                    return CELLVIEW_FOCUSED_BORDER_PAINT;

                case NORMAL:
                default:
                    return CELLVIEW_NORMAL_BORDER_PAINT;
            }
        }

        protected void onDraw(Canvas canvas) {

//            Draw background
            canvas.drawRect(x, y, x + cellWidth, y + cellHeight, getBackgroundPaint());

//            Draw Border
            drawBorder(canvas);

//            Draw Character
            canvas.drawText(c, x + (cellWidth / 2), y + cellHeight - 5, CELLVIEW_TEXT_FONT_PAINT);

//            Show the clue number if a valid one was set.
            if (clueNumber > 0)
                canvas.drawText(String.valueOf(clueNumber), x + 3, y + 12, CELLVIEW_NUMBER_FONT_PAINT);

        }

        private void drawBorder(Canvas canvas) {
            canvas.drawLine(x, y, x + cellWidth, y, getBorderPaint()); //Top border
            canvas.drawLine(x + cellWidth, y, x + cellWidth, y + cellHeight, getBorderPaint()); //Right border
            canvas.drawLine(x, y + cellHeight, x + cellWidth, y + cellHeight, getBorderPaint()); //Bottom border
            canvas.drawLine(x, y, x, y + cellHeight, getBorderPaint()); //Left Border
        }

        public boolean equals(CellView view) {
            if (view == null)
                return false;

            return view.x == x && view.y == y;
        }

        @Override
        public boolean equals(Object o) {
            CellView v = (CellView) o;
            if (v == null)
                return false;
            return equals(v);
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder(5);
            return builder.append("Cell [").append("x: ").
                    append(x).append(", y:").append(y).
                    append(", char:").append(c).append("]").toString();
        }


    }
}
