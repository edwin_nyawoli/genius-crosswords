package com.edwinnyawoli.geniuscrosswords;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.edwinnyawoli.geniuscrosswords.adapter.CluesRecyclerAdapter;
import com.edwinnyawoli.geniuscrosswords.adapter.PuzzleManager;
import com.edwinnyawoli.geniuscrosswords.model.Grid;
import com.edwinnyawoli.geniuscrosswords.utils.PerSecondTimer;
import com.edwinnyawoli.geniuscrosswords.utils.TimeUtils;
import com.edwinnyawoli.geniuscrosswords.view.CrosswordGridView;
import com.edwinnyawoli.geniuscrosswords.view.CrosswordKeyboardView;

public class CrosswordsActivity extends AppCompatActivity implements PerSecondTimer.OnTimeChangedListener {
    private static final String TAG = "CrosswordActivity";
    private static final String IS_FIRST_RUN = "first_run";
    private Grid mGrid;
    private PuzzleManager mPuzzleManager;
    private String mCollection;
    private int mLevel;
    private CrosswordGridView gridView;
    private CrosswordKeyboardView keyboardView;
    private CrosswordGridView.CellView mFocusedCellView;
    private DrawerLayout drawerLayout;
    private boolean updateTimer;
    private TextView mTimerTextView;
    private long mStartTime, mStopTime, mElapsedTime;
    private int mTimeCounter;
    private PerSecondTimer mTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        TODO: Find a way to save state when the user leaves this puzzle.
//        TODO: Find a way to alert the user whether the characters entered to solve a clue are correct or not

        mPuzzleManager = PuzzleManager.getInstance(this);
        Intent data = getIntent();
        if (data != null) {
            mLevel = data.getIntExtra("LEVEL", 1);
            mCollection = data.getStringExtra("COLLECTION");
            mGrid = mPuzzleManager.getCrosswordGrid(mCollection, mLevel);
        }

        drawerLayout = (DrawerLayout) LayoutInflater.from(this).inflate(R.layout.activity_crosswords, null, false);
        setContentView(drawerLayout);

        mTimerTextView = (TextView) findViewById(R.id.timerTextView);
//        ImageView gridBackgroundView = (ImageView) findViewById(R.id.grid_background);
//        gridBackgroundView.setImageResource(R.drawable.background_fashion_2);
        mTimeCounter = -1;
        mTimer = new PerSecondTimer();
        mTimer.setOnTimeChangedListener(this);

        gridView = (CrosswordGridView) findViewById(R.id.crossword_grid_view);
        gridView.setGrid(mGrid);

        keyboardView = new CrosswordKeyboardView(this, R.id.keyboard_view, R.xml.keyboard);

        initListeners();
        hintDrawerOnFirstVisit();
    }

    private void hintDrawerOnFirstVisit() {
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        boolean isFirstRun = prefs.getBoolean(IS_FIRST_RUN, true);
        if (isFirstRun) {
            drawerLayout.openDrawer(GravityCompat.END);
            prefs.edit().putBoolean(IS_FIRST_RUN, false).apply();
        }
    }

    private void performTimerUpdate() {
        if (!updateTimer)
            return;

        mTimeCounter += 1000;
        mTimerTextView.setText(TimeUtils.format(mTimeCounter, false));
    }

    private void initListeners() {
        gridView.setCellViewFocusListener(new CrosswordGridView.CellViewFocusListener() {
            @Override
            public void onFocusChange(CrosswordGridView.CellView lostFocusView, CrosswordGridView.CellView gainedFocusView) {
                mFocusedCellView = gainedFocusView;
            }
        });


        keyboardView.setOnKeyPressListener(new CrosswordKeyboardView.OnKeyPressListener() {
            @Override
            public void onKey(int code) {
                if (mFocusedCellView != null) {
                    if (code == CrosswordKeyboardView.KEYCODE_DEL)
                        mFocusedCellView.clearCharacter();
                    else
                        mFocusedCellView.setCharacter((char) code);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateTimer = true;
        mTimer.start();
        mStartTime = System.nanoTime();
    }

    @Override
    protected void onPause() {
        super.onPause();
        updateTimer = false;
        mTimer.stop();
        mElapsedTime = System.nanoTime() - mStartTime;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mStopTime = System.nanoTime();
        mElapsedTime = mStopTime - mStartTime;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_crossword_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cluesToggle:
                toggleDrawerVisibility();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggleDrawerVisibility() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END))
            drawerLayout.closeDrawer(GravityCompat.END);
        else
            drawerLayout.openDrawer(GravityCompat.END);
    }

    @Override
    public void onTimeChanged() {
        performTimerUpdate();
    }

    public static class CluesFragment extends Fragment {
        CrosswordsActivity mActivity;

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            mActivity = ((CrosswordsActivity) context);
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_clues, container, false);
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            RecyclerView cluesRecyclerView = (RecyclerView) view.findViewById(R.id.clues_recycler_view);

            Grid grid = PuzzleManager.getInstance(mActivity).getCrosswordGrid(mActivity.mCollection, mActivity.mLevel);
            CluesRecyclerAdapter adapter = new CluesRecyclerAdapter(mActivity, grid.getWordList());
            cluesRecyclerView.setHasFixedSize(true);
            cluesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            cluesRecyclerView.setAdapter(adapter);
        }
    }
}
