package com.edwinnyawoli.geniuscrosswords.utils;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;

/**
 * Created by ed on 5/12/2016.
 * A timer that calls an interface method every second to allow operations to be performed
 * on a secondly basis.
 */
public class PerSecondTimer {
    private Handler mHandler;
    private OnTimeChangedListener l;
    private final Runnable mTicker = new Runnable() {
        public void run() {
            onTimeChanged();

            long now = SystemClock.uptimeMillis();
            long next = now + (1000 - now % 1000);

            mHandler.postAtTime(mTicker, next);
        }
    };

    public PerSecondTimer() {
        mHandler = new Handler(Looper.getMainLooper());
    }

    private void onTimeChanged() {
        if (l != null)
            l.onTimeChanged();
    }

    public void setOnTimeChangedListener(OnTimeChangedListener l) {
        this.l = l;
    }

    public void stop() {
        mHandler.removeCallbacks(mTicker);
    }

    public void start() {
        mTicker.run();
    }

    public interface OnTimeChangedListener {
        void onTimeChanged();
    }
}
