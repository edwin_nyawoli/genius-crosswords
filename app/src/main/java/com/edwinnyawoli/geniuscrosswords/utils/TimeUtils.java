package com.edwinnyawoli.geniuscrosswords.utils;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by ed on 5/12/2016.
 */
public class TimeUtils {
    private static final DateFormat dateFormat;

    static {
        dateFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM);
    }

    public static final String format(Date date) {
        return dateFormat.format(date);
    }

    public static final String format(long date) {
        return dateFormat.format(new Date(date));
    }

    public static final String format(Date date, boolean includeAMPM) {
        if (!includeAMPM) {
            String dateString = dateFormat.format(date).split(" ")[0];
            return dateString.substring(dateString.indexOf(':') + 1);
        }
        return dateFormat.format(date);
    }

    public static final String format(long date, boolean includeAMPM) {
        return format(new Date(date), includeAMPM);
    }
}
