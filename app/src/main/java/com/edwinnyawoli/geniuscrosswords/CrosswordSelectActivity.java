package com.edwinnyawoli.geniuscrosswords;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.edwinnyawoli.geniuscrosswords.adapter.ArrayAdapter;

public class CrosswordSelectActivity extends AppCompatActivity implements ArrayAdapter.OnItemClickListener {
    private String mCollection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crossword_select);

        String[] puzzles = new String[]{
                "Puzzle #1",
                "Puzzle #2",
                "Puzzle #3",
                "Puzzle #4",
                "Puzzle #5",
                "Puzzle #6",
                "Puzzle #7",
                "Puzzle #8",
                "Puzzle #9",
                "Puzzle #10",
        };
        TextView selectTextView = (TextView) findViewById(R.id.select_crossword_text_view);
        RecyclerView puzzleRecyclerView = (RecyclerView) findViewById(R.id.puzzle_recycler_view);
        Intent data = getIntent();
        if (data != null) {
            mCollection = data.getStringExtra("COLLECTION");
            selectTextView.setText(String.format(getString(R.string.crossword_select), mCollection.toLowerCase()));
        }

        ArrayAdapter<String> puzzleArrayAdapter = new ArrayAdapter<String>(this, puzzles);
        puzzleRecyclerView.setAdapter(puzzleArrayAdapter);
        puzzleRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        puzzleArrayAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(View view, int pos) {
        Intent crosswordIntent = new Intent(CrosswordSelectActivity.this, CrosswordsActivity.class);
        crosswordIntent.putExtra("LEVEL", ++pos);
        crosswordIntent.putExtra("COLLECTION", mCollection);
        startActivity(crosswordIntent);
    }
}
