package com.edwinnyawoli.geniuscrosswords.model;

/**
 * Created by ed on 4/14/2016.
 */
public class Word {
    private int startX;
    private int startY;
    private String word;
    private String clue;
    private boolean isHorizontal;
    private int wordNumber;
    private int clueNumber;

    public Word(String word, String clue, int startX, int startY, boolean isHorizontal, int wordNumber) {
        this.word = word.toUpperCase();
        this.clue = clue;
        this.startX = startX;
        this.startY = startY;
        this.isHorizontal = isHorizontal;
        this.wordNumber = wordNumber;
        this.clueNumber = -1;
    }

    public int getStartingX() {
        return startX;
    }

    public int getStartingY() {
        return startY;
    }

    public boolean isHorizontal() {
        return isHorizontal;
    }

    public String getClue() {
        return clue;
    }

    public String getWord() {
        return word;
    }

    public int getLength() {
        return word.length();
    }

    /**
     * The index of the word in the wordlist.
     *
     * @return
     */
    public int getWordNumber() {
        return wordNumber;
    }

    public int getClueNumber() {
        return clueNumber;
    }

    public void setClueNumber(int clueNumber) {
        this.clueNumber = clueNumber;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(word.length() + 30);
        builder.append(isHorizontal).
                append(',').append("x:").append(startX).
                append(',').append("y:").append(startY);
        return builder.toString();
    }
}
