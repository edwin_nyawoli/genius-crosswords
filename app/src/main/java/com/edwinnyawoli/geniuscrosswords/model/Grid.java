package com.edwinnyawoli.geniuscrosswords.model;

import java.util.ArrayList;

/**
 * Created by ed on 4/14/2016.
 */
public class Grid {
    private int columnCount;
    private int rowCount;
    private ArrayList<Word> wordList;

    public Grid(ArrayList<Word> wordList, int rowCount, int columnCount) {
        this.wordList = wordList;
        this.columnCount = rowCount;
        this.rowCount = columnCount;
    }

    public int getRowCount() {
        return rowCount;
    }

    public int getColumnCount() {
        return columnCount;
    }

    public ArrayList<Word> getWordList() {
        return wordList;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("[").append(rowCount).append(" x ").append(columnCount).append("] Grid").toString();
    }
}
