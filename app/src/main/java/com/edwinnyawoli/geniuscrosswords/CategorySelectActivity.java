package com.edwinnyawoli.geniuscrosswords;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class CategorySelectActivity extends AppCompatActivity implements View.OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_select);
        findViewById(R.id.button_collection_fashion).setOnClickListener(this);
        findViewById(R.id.button_collection_technology).setOnClickListener(this);
        findViewById(R.id.button_collection_politics).setOnClickListener(this);
        findViewById(R.id.button_collection_sports).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent crosswordSelectIntent = new Intent(this, CrosswordSelectActivity.class);
        crosswordSelectIntent.putExtra("COLLECTION", ((Button) view).getText());
        startActivity(crosswordSelectIntent);
    }
}
