package com.edwinnyawoli.geniuscrosswords;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MenuActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        findViewById(R.id.button_play).setOnClickListener(this);
        findViewById(R.id.button_credits).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_play:
                startActivity(new Intent(this, CategorySelectActivity.class));
                break;

            case R.id.button_credits:
                startActivity(new Intent(this, CreditsActivity.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener exitClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case DialogInterface.BUTTON_POSITIVE:
                        finish();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        dialogInterface.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exit?");
        builder.setMessage("Leaving so soon?");
        builder.setPositiveButton("Yes", exitClickListener);
        builder.setNegativeButton("No", exitClickListener);
        builder.create().show();
    }
}
