package com.edwinnyawoli.geniuscrosswords.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.edwinnyawoli.geniuscrosswords.R;
import com.edwinnyawoli.geniuscrosswords.model.Word;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by ed on 4/15/2016.
 */
public class CluesRecyclerAdapter extends RecyclerView.Adapter<CluesRecyclerAdapter.ClueViewHolder> {
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_HEADER = 1;
    private final String CLUES_ACROSS;
    private final String CLUES_DOWN;

    private LinkedList<Word> acrossList;
    private LinkedList<Word> downList;
    private int size;
    private Context mContext;

    public CluesRecyclerAdapter(Context context, ArrayList<Word> wordList) {
        mContext = context;
        size = wordList.size();
        if (size > 0) {
            acrossList = new LinkedList<>();
            downList = new LinkedList<>();

            for (int i = 0; i < size; i++) {
                Word word = wordList.get(i);
                if (word.isHorizontal())
                    acrossList.add(word);
                else
                    downList.add(word);
            }
        }

        CLUES_ACROSS = context.getString(R.string.clues_across);
        CLUES_DOWN = context.getString(R.string.clues_down);
    }

    @Override
    public ClueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER)
            return new ClueViewHeaderHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_item_clue_header, parent, false));
        return new ClueViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_item_clue, parent, false));
    }

    @Override
    public void onBindViewHolder(ClueViewHolder holder, int position) {
        if (holder.getItemViewType() == TYPE_HEADER)
            ((ClueViewHeaderHolder) holder).mHeaderTextView.setText(getViewHeader(position));

        Word word = getWord(position);
        holder.mClueNumTextView.setText("" + word.getClueNumber());
        holder.mClueTextView.setText(word.getClue());
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 || (position) == acrossList.size())
            return TYPE_HEADER;
        return TYPE_ITEM;
    }

    private String getViewHeader(int position) {
        if (position < acrossList.size())
            return CLUES_ACROSS;
        return CLUES_DOWN;
    }

    private Word getWord(int pos) {
        if (pos < acrossList.size())
            return acrossList.get(pos);
        return downList.get(pos - acrossList.size());
    }

    @Override
    public int getItemCount() {
        return size;
    }

    public class ClueViewHeaderHolder extends ClueViewHolder {
        TextView mHeaderTextView;

        public ClueViewHeaderHolder(View view) {
            super(view);
            mHeaderTextView = (TextView) view.findViewById(R.id.clue_header);
        }
    }

    public class ClueViewHolder extends RecyclerView.ViewHolder {
        TextView mClueNumTextView;
        TextView mClueTextView;

        public ClueViewHolder(View view) {
            super(view);
            mClueNumTextView = (TextView) view.findViewById(R.id.clue_number);
            mClueTextView = (TextView) view.findViewById(R.id.clue);
        }
    }
}
