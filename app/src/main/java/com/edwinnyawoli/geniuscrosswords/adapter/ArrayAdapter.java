package com.edwinnyawoli.geniuscrosswords.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.edwinnyawoli.geniuscrosswords.R;

import java.util.ArrayList;

/**
 * Created by ed on 4/23/2016.
 */
public class ArrayAdapter<T> extends RecyclerView.Adapter<ArrayAdapter.ViewHolder> {
    private static final int BUTTON_TYPE_1 = 0;
    private static final int BUTTON_TYPE_2 = 1;
    private static final String TAG = "ArrayAdapter";

    private final LinearLayout.LayoutParams params;
    private OnItemClickListener itemClickListener;
    private T[] data;
    private Context mContext;

    public ArrayAdapter(Context context, ArrayList<T> data) {
        this(context, (T[]) data.toArray());
    }

    public ArrayAdapter(Context context, T[] data) {
        this.data = data;
        mContext = context;

        params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                mContext.getResources().getDimensionPixelSize(R.dimen.button_crossword_select_height)
        );
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Button button = new Button(mContext);

        button.setLayoutParams(params);
        button.setAllCaps(false);

        switch (viewType) {
            case BUTTON_TYPE_1:
                button.setTextColor(Color.WHITE);
                button.setBackgroundColor(Color.rgb(72, 72, 72));
                break;

            case BUTTON_TYPE_2:
                button.setTextColor(Color.BLACK);
                button.setBackgroundColor(Color.rgb(215, 215, 215));
                break;
        }

        return new ViewHolder(button, itemClickListener);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.button.setText(data[position].toString());
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.length;
    }

    @Override
    public int getItemViewType(int position) {
        return position % 2;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int pos);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        Button button;

        public ViewHolder(View view, final OnItemClickListener l) {
            super(view);
            button = (Button) view;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (l != null)
                        l.onItemClick(view, getAdapterPosition());
                }
            });

        }
    }
}
