package com.edwinnyawoli.geniuscrosswords.adapter;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.edwinnyawoli.geniuscrosswords.model.Grid;
import com.edwinnyawoli.geniuscrosswords.model.Word;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by ed on 4/16/2016.
 */
public class PuzzleManager {
    private static PuzzleManager mInstance;
    private Context mContext;

    private PuzzleManager(Context context) {
        mContext = context.getApplicationContext();
    }

    public static synchronized PuzzleManager getInstance(Context context) {
        if (mInstance == null)
            mInstance = new PuzzleManager(context);
        return mInstance;
    }

    public Grid getCrosswordGrid(String collection, int level) {
        return readPuzzle(collection, level);
    }

    private Grid readPuzzle(String collection, int level) {
        Grid grid = null;
        AssetManager assetManager = mContext.getAssets();
        try {
            final String puzzleLoc = "puzzles/" + collection.toLowerCase() + "/" + level + ".json";
            final StringBuilder builder = new StringBuilder(2000);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(assetManager.open(puzzleLoc)));
            String readStr = null;
            do {
                readStr = bufferedReader.readLine();
                builder.append(readStr);
            } while (readStr != null);
            bufferedReader.close();

            grid = generateGridFromSource(builder.toString());
//            reader = new PuzzleReader(
//                    new InputStreamReader(
//                            assetManager.open(puzzleLoc)
//                    ));


//            grid = reader.read();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("PuzzleManager", "An error occurred...");
        }
        Log.d("PuzzleManager", "Grid is null: " + (grid == null));
        return grid;
    }

    private Grid generateGridFromSource(String src) {
        Grid grid = null;
        try {
            JSONObject root = new JSONObject(src);
            ArrayList<Word> wordList = new ArrayList<>(16);
            JSONArray wordListArray = root.getJSONObject("puzzle").getJSONArray("wordlist");
            int maxColumn = 0;
            int maxRow = 0;
            int clueCounter = 0;
            for (int i = 0; i < wordListArray.length(); i++) {
                JSONObject wordObject = wordListArray.getJSONObject(i);
                int startX = wordObject.getInt("startX");
                int startY = wordObject.getInt("startY");
                Word word = new Word(
                        wordObject.getString("word"),
                        wordObject.getString("clue"),
                        startX,
                        startY,
                        wordObject.getBoolean("isHorizontal"),
                        i + 1
                );

                boolean clueNumberSet = false;
                for (int j = 0; j < i; j++) {
                    Word wordAtJ = wordList.get(j);
                    if (wordAtJ.getStartingX() == startX && wordAtJ.getStartingY() == startY) {
                        word.setClueNumber(wordAtJ.getClueNumber());
                        clueNumberSet = true;
                        break;
                    }
                }

                if (!clueNumberSet) {
                    word.setClueNumber(++clueCounter);
                }

                if (startX > maxColumn)
                    maxColumn = startX;
                if (startY > maxRow)
                    maxRow = startY;

                wordList.add(word);
            }

            grid = new Grid(wordList, ++maxRow, ++maxColumn);

            //Assign ClueNumbers to the words
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return grid;
    }
}
